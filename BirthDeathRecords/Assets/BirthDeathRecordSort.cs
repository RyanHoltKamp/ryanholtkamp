﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

///////////////////////////////////////////////////////////////////////////////////////
//attach this to any game object in unity.  
//Upon start, it will create a CSV file with randomly generated birth/death years
//then parse the data to find the year the most people were alive
//results are exported to a text file.
///////////////////////////////////////////////////////////////////////////////////////

public class BirthDeathRecordSort : MonoBehaviour 
{

	SingleYearRecord[] YearlyTotals= new SingleYearRecord[101];  //array 0f 101 indexes representing years 1900 to 2000
	List<BirthDeathRecord> LifeSpans = new List<BirthDeathRecord>(); //contains records of each person's birth and death
	int MaxAliveYear = 0;  //the resulting Year with the most living people
	int MaxAliveSum = 0;   //the resulting count of people living in the year
		
	const int RECORDCOUNT = 2000; //total number of records to generate and process



	void Start ()
	{
		//creates a CSV file with comma delimited Birth/Death years per row - randomly generated
		GenerateCSVFile ("BirthDeathYears.csv");  

		//Read the CSV file into the "Records" list of Birth and Death years
		ReadCSVFile("BirthDeathYears.csv");

		//Parse the data 
		ParseData();

		//Save the results to a text file
		ExportResults ("Results.txt");

	}
		


	//create a CSV file with comma delimited Birth/Death years 
	void GenerateCSVFile(string file)
	{
		var f = File.CreateText(file);
		for(int i=0;i<RECORDCOUNT;i++)
		{
			//create a birth date between 1900 and 2000
			int birth=1900+Random.Range(0,100);
			//create a death date with a lifespan up to 80 years
			int death=birth+Random.Range(0,80);

			//assert Death is on or before the year 2000
			if (death>2000) 
				death=2000;

			//write line to CSV file with birth and death separated by comma
			f.WriteLine(birth+","+death);
		}
		f.Close();
	}



	//read the contents of the CSV file into the LifeSpans List.  Each Record contains Birth and Death Years
	void ReadCSVFile(string file)
	{
		var f = File.OpenText(file);
		var line = f.ReadLine();
		while(line != null)
		{
			//parse each line from the CSV File into a string Array with two elements
			string[] BirthDeath=line.Split(new string[]{","},System.StringSplitOptions.None);

			//store it in the LifeSpans list (BirthDeath element 0 = birth, 1 = death after call to "Split")
			LifeSpans.Add(new BirthDeathRecord(int.Parse(BirthDeath[0]),int.Parse(BirthDeath[1])));

			line = f.ReadLine();
		}  	 
	}



	void ParseData()
	{
		//Initialize Records
		for(int yr=0; yr<YearlyTotals.Length; yr++)
			YearlyTotals[yr] = new SingleYearRecord ();
		
		//for each record/person
		//Store Values in YearlyTotals array - counts births, deaths, count of living people for each year
		for(int life=0; life<LifeSpans.Count; life++)
			ProcessRecord (LifeSpans[life].BirthYear, LifeSpans[life].DeathYear);

		//Now YearlyTotals has sum of births, deaths, and sum of living for each year
		//parse YearlyTotals, find the year with highest number of living
		//store in MaxAliveSum and MaxAliveYear
		for( int yr=0; yr<101; yr++)
			if (YearlyTotals[yr].NumAlive >= MaxAliveSum) 
			{
				MaxAliveSum = YearlyTotals[yr].NumAlive;
				MaxAliveYear = yr+1900; //add 1900 since array indexes 0-100 correspond with yrs 1900-2000
			}
	}


	//ParseData Helper Function
	//takes a single birth and death record, increments birth and death in YearlyTotals
	//also increments NumAlive for every year the person was alive
	void ProcessRecord(int birthYr, int deathYr)
	{
		birthYr -= 1900;//subtract 1900 since array elements 0-100 correspond to yrs 1900-2000
		deathYr -= 1900;

		//increment counts of births and deaths -extra data for final report
		YearlyTotals[birthYr].NumBorn++;
		YearlyTotals[deathYr].NumDied++;

		//increment the annual counts for each year the person lived (death year included in 'alive' calculation)
		for (int yr = birthYr; yr <=  deathYr; yr++) 
		{
			YearlyTotals[yr].NumAlive++; 
		}	
	}




	//save the results in a text file
	void ExportResults(string file)
	{
		var f = File.CreateText(file);
		f.WriteLine ("The year with the most living people was: " + MaxAliveYear);
		f.WriteLine ("The number of living people in "+MaxAliveYear+" was: " + MaxAliveSum + "\n\r\n\r");

		f.WriteLine ("METRICS PER YEAR:");
		for(int Year=0;Year<101;Year++)
			f.WriteLine(1900+Year+": Born: "+Pad(YearlyTotals[Year].NumBorn)
				+"  Alive: "+Pad(YearlyTotals[Year].NumAlive)
				+"  Died: "+Pad(YearlyTotals[Year].NumDied) );
		
		f.Close();
	}


	//adds spaces after a number for clarity in resulting report. (columns line up better aesthetically)
	string Pad(int s)
	{
		string result= s + "";
		while (result.Length < 3)
			result = result+" ";
		return (result);
	}
}


//class to contain a record for an individual person's birth and death year
public class BirthDeathRecord
{
	public int BirthYear;	
	public int DeathYear;

	public BirthDeathRecord(int Birth, int Death)
	{
		BirthYear = Birth;
		DeathYear = Death;
	}
}

//class to contain counts of births, deaths, living people for a single year
public class SingleYearRecord
{
	public int NumDied;
	public int NumBorn;
	public int NumAlive;

	public SingleYearRecord()
	{
		NumDied = 0;
		NumBorn = 0;
		NumAlive = 0;
	}
}